## Sample Usage:

``` javascript
(function($) {

        $('#targetElement').bindMediaQuery('desktop', {
                onEnter: function() {

                        $(this).css({color: 'red'});
                },
                onLeave: function() {

                        $(this).css({color: 'black'});
                }
        });
        
        
        
        $('ul li').bindMediaQuery('desktop', {
                onEnter: function(e) {
                        //console.log('onEnter','desktop', e);
                        $(this).stop().animate({"margin-left": 0});
                },
                onLeave: function(e) {
                        //console.log('onLeave','desktop', e);
                }
        }).bindMediaQuery('desktop-large', {
                onEnter: function(e) {
                        //console.log('onEnter','desktop-large',e);
                        $(this).stop().animate({"margin-left": 500});
                }
        });


        // global events bind on media-query
        $.nmMediaQuery.bind('desktop', {
                onEnter: function() {
                        //console.log(this, arguments);
                },
                onLeave: function() {
                        //console.log(this, arguments);
                }
        }).bind('desktop-large', {
                onEnter: function() {
                        //console.log(this, arguments);
                },
                onLeave: function() {
                        //console.log(this, arguments);
                }
        });

})(jQuery);
```