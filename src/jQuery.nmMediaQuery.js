/*!
 * jQuery nmMediaQuery Plugin
 * http://www.netzmodule.de
 * 
 * Copyright (c) netzmodule.de 2012-2014 M. Braun
 * Version: 0.1.1 
 * 
 * Licensed under the MIT licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Requires: jQuery v1.7.0 or later
 * 
 */

(function($) {
        
        "use strict";
        
        $.nmMediaQuery = {
                
        };
        
        /**
         * Configuration
         * 
         * @type Object
         */
        var config = {
                'helperId': 'nm-media-query',
                'mediaClassNameSelector' : 'body, div',
                'enableMediaClassName' : true
        };
        
        $.nmMediaQuery.setConfig = function(options){
                if( $.isPlainObject(options) ){ 
                        $.extend(config, options);
                }
        };
        
        /**
         * Global registry.
         * 
         * @type Objeckt
         */
        var MediaQueryRegistry = {};

        /**
         * Current media query name.
         * 
         * @type {string}
         */
        var current = null;

        /**
         * 
         * 
         * @type {Element}
         */
        var MediaQueryElement = $('<div id="'+config.helperId+'" style="display:none; position: absolute; top: -1000px; width: 1px; height: 1px;">');

        /**
         * Element registry. 
         * 
         * @type Array
         */
        var Elements = [];
        
        /**
         * Resize handler.
         *  
         * @returns {void}
         */
        var resize = function() {

                var c = new String(MediaQueryElement.css('content'));

                // remove chars
                c = c.replace(/'/gi, '');
                c = c.replace(/"/gi, '');

                if (c !== current) {
                        
                        //console.log(current, c)
                        if( config.enableMediaClassName === true ){
                                $(config.mediaClassNameSelector).removeClass(current).addClass(c);
                        }

                        if (MediaQueryRegistry[current] && $.isFunction(MediaQueryRegistry[current].onLeave)) {
                                var e = jQuery.Event("onLeave", {code:c});
                                MediaQueryRegistry[current].onLeave.apply(e, [current]);

                                
                        }

                        if (current !== null && Elements[current]) {
                                
                                $(Elements[current]).each(function(index) {
                                        
                                        var el = Elements[current][index];
                                        
                                        if( $.isPlainObject(el) && $.isPlainObject(el.settings)  ){
                                                
                                                if( $.isFunction(el.settings.onLeave) ){
                                                        var e = jQuery.Event("onLeave", {code:current});
                                                       
                                                        el.settings.onLeave.apply(el.$el,[e]);
                                                }
                                                
                                        }

                                });
                        }

                        if (MediaQueryRegistry[c] && $.isFunction(MediaQueryRegistry[c].onEnter)) {
                                var e = jQuery.Event("onEnter", {code:current});
                                MediaQueryRegistry[c].onEnter.apply(e, [c]);

                        }

                        if (c !== null && Elements[c]) {
                                
                                $(Elements[c]).each(function(index) {
                                        
                                        var el = Elements[c][index];
                                       
                                        if( $.isPlainObject(el) && $.isPlainObject(el.settings)  ){
                                                                                                 
                                                if( $.isFunction(el.settings.onEnter) ){
                                                        var e = jQuery.Event("onEnter", {code:c} );
                                                        el.settings.onEnter.apply(el.$el,[e]);
                                                }
                                                
                                        }

                                });
                        }

                        current = c;
                }

        };

        /**
         * 
         * @param {type} code
         * @param {type} opt
         * @returns {Nm.MediaQuery}
         */
        $.nmMediaQuery.bind = function(code, opt) {
                // defaults
                var options = {
                        onEnter: function() {
                        },
                        onLeave: function() {
                        }
                };

                $.extend(options, opt);

                if (!MediaQueryRegistry[code]) {
                        MediaQueryRegistry[code] = {};
                }
                if ($.isFunction(options.onEnter)) {
                        MediaQueryRegistry[code].onEnter = (options.onEnter);
                }
                if ($.isFunction(options.onLeave)) {
                        MediaQueryRegistry[code].onLeave = (options.onLeave);
                }

                return this;
        };


        /**
         * jQuery Plugin 
         * 
         * @param {string} code     name of media query
         * @param {type} options    
         * @returns {unresolved}
         */
        $.fn.bindMediaQuery = function(code, options) {

                var self = this;
                
                var settings = {
                        onEnter: function(){},
                        onLeave: function(){}
                };

                $.extend(settings, options);

                return $(this).each(function() {

                        if (!Elements[code]) {
                                Elements[code] = [];
                        }

                        Elements[code].push({$el: $(this), el: this, code: code, settings: settings});

                });
        };



        $(document).ready(function() {
                $('body').append(MediaQueryElement);
                resize();
                $(window).resize(resize);
        });



})(jQuery);